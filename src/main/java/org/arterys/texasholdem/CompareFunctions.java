package org.arterys.texasholdem;

import java.util.Comparator;
import java.util.Map.Entry;

public class CompareFunctions {

	/**
	 * Sorts players by their specialty hand in descending order.
	 */
	static class SortPlayersByScore implements Comparator<Player> {
		public int compare(Player a, Player b) {
			int cmp = 0;

			if (cmp == 0) {
				cmp = b.getHighestSpecialty() - a.getHighestSpecialty();
			}

			// a tie has occurred
			switch (b.getHighestSpecialty()) {
			case 10:
				// In a Royal Flush, all cards are equal.
				break;

			case 9:
				// In a Straight Flush, the highest card wins the tie.
				// If highest card is equal, they tie.
				if (cmp == 0) {
					cmp = b.getHighestCard() - a.getHighestCard();
				}
				break;

			case 8:
				// In a Four of a Kind, one kicker wins the tie.
				if (cmp == 0) {
					cmp = b.getHighestCard() - a.getHighestCard();
				}

				// If kicker is equal, they tie.
				if (cmp == 0) {
					cmp = b.getKickers().get(0) - a.getKickers().get(0);
				}
				break;

			case 7:
				// In a Full House, compare the highest triple card.
				if (cmp == 0) {
					cmp = b.getHighestCard() - a.getHighestCard();
				}

				// If there is a tie, compare the second pair card.
				if (cmp == 0) {
					cmp = b.getSecondPairCard() - a.getSecondPairCard();
				}
				break;

			case 6:
				// In a flush, compare the 5 highest flush cards.
				// If all the card match, there is a tie.
				Integer[] bFlush = b.getFlushCards();
				Integer[] aFlush = a.getFlushCards();
				for (int i=0; i<5; i++) {
					if (cmp == 0) {
						cmp = bFlush[i] - aFlush[i];
					}
				}
				break;

			case 5:
				// In a Straight, the highest card wins the tie.
				// If the highest cards are equal, they tie.
				if (cmp == 0) {
					cmp = b.getHighestCard() - a.getHighestCard();
				}
				break;

			case 4:
				// In a Three of a Kind, the highest triple wins the tie.
				if (cmp == 0) {
					cmp = b.getHighestCard() - a.getHighestCard();
				}
				
				// Otherwise, iterate through 2 kickers to break a tie.
				// If all kickers are equal, they tie.
				if (cmp == 0) {
					cmp = b.getKickers().get(0) - a.getKickers().get(0);
				}
				if (cmp == 0) {
					cmp = b.getKickers().get(1) - a.getKickers().get(1);
				}
				break;

			case 3:
				// If there are two pairs, the highest pair number wins.
				if (cmp == 0) {
					cmp = b.getHighestCard() - a.getHighestCard();
				}
				
				// If the highest pair numbers tie, check the second pair.
				if (cmp == 0) {
					cmp = b.getSecondPairCard() - a.getSecondPairCard();
				}
				
				// If the second pair numbers tie, check one kicker.
				// If the kickers are equal, they tie.
				if (cmp == 0) {
					cmp = b.getKickers().get(0) - a.getKickers().get(0);
				}
				break;

			case 2:
				// If there is only one pair, the highest pair wins.
				if (cmp == 0) {
					cmp = b.getHighestCard() - a.getHighestCard();
				}
				
				// Otherwise, iterate through 3 kickers to break a tie.
				// If all kickers are equal, they tie.
				if (cmp == 0) {
					cmp = b.getKickers().get(0) - a.getKickers().get(0);
				}
				if (cmp == 0) {
					cmp = b.getKickers().get(1) - a.getKickers().get(1);
				}
				if (cmp == 0) {
					cmp = b.getKickers().get(2) - a.getKickers().get(2);
				}
				break;
				
			case 1:
				// If there is just one highest card, that card wins if it is the highest.

				// Otherwise, iterate through 4 kickers to break a tie.
				// If all kickers are equal, they tie.
				if (cmp == 0) {
					cmp = b.getKickers().get(0) - a.getKickers().get(0);
				}
				if (cmp == 0) {
					cmp = b.getKickers().get(1) - a.getKickers().get(1);
				}
				if (cmp == 0) {
					cmp = b.getKickers().get(2) - a.getKickers().get(2);
				}
				if (cmp == 0) {
					cmp = b.getKickers().get(3) - a.getKickers().get(3);
				}
				break;
			}

			return cmp;
		}
	}

	/**
	 * Sort a hashmap of card numbers and card counts in descending order
	 * by card numbers.
	 */
	static class SortCardNumbersByKey implements Comparator<Entry<Integer, Integer>> {
		public int compare(Entry<Integer, Integer> a, Entry<Integer, Integer> b) {
			return b.getKey().compareTo(a.getKey());
		}
	}
}
