package org.arterys.texasholdem;

import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Player {
	private String name;
	private int highestCard = -1;
	private int bestFiveCardRank = 1;
	private int secondPairCard = -1;
	private Integer[] flush;
	private HashMap<Integer, Integer> cardCount = new HashMap<Integer, Integer>();
	private ArrayList<Integer> kickers = new ArrayList<Integer>();
	
	/**
	 * Creates the player with a certain name.
	 * 
	 * @param playerCards name of the player.
	 * @param communityCards list of cards that all players can use.
	 */
	public Player(String[] playerCards, String[] communityCards) {
		this.name = playerCards[0];
		
    	// Sort all the cards by suit and determine if 5 cards create a flush.
    	TreeSet<Integer> spades = new TreeSet<>();
    	TreeSet<Integer> hearts = new TreeSet<>();
    	TreeSet<Integer> clubs = new TreeSet<>();
    	TreeSet<Integer> diamonds = new TreeSet<>();
    	
    	// A sorted list of all the cards, in a unique list.
    	TreeSet<Integer> uniqueCardNumbers = new TreeSet<>();
    	
    	// Sort community cards by face value and loads into a list for comparisons.
    	for (String card : communityCards) {
    		Integer number = CardUtils.convertCardNumberToInteger(card.substring(0,1));
    		String face = card.substring(1,2).toLowerCase();
    		
    		if (face.equals("s")) spades.add(number);
    		else if (face.equals("h")) hearts.add(number);
    		else if (face.equals("c")) clubs.add(number);
    		else diamonds.add(number);
    		
    		// Add the card to be sorted at a future time
    		countCards(number);
    		uniqueCardNumbers.add(number);
    	}
    	
    	// Sort player's card by face value and loads into a list for comparisons.
    	for (int i=1; i<playerCards.length; i++) {
    		Integer number = CardUtils.convertCardNumberToInteger(playerCards[i].substring(0,1));
    		String face = playerCards[i].substring(1,2).toLowerCase();
    		
    		if (face.equals("s")) spades.add(number);
    		else if (face.equals("h")) hearts.add(number);
    		else if (face.equals("c")) clubs.add(number);
    		else diamonds.add(number);
    		
    		// Add the card to be sorted at a future time
    		countCards(number);
    		uniqueCardNumbers.add(number);
    	}
    	
    	// Sort all the cards in the count in descending order by key (card number)
    	
    	// If any faces have 5 or more cards, it is immediately set
    	// as a Flush (Specialty #6).
    	if (spades.size() >= 5) {
    		setFlushCards(spades);
    		determineStraight(spades, true);
    	} else if (hearts.size() >= 5) {
    		setFlushCards(hearts);
    		determineStraight(hearts, true);
    	} else if (clubs.size() >= 5) {
    		setFlushCards(clubs);
    		determineStraight(clubs, true);
    	} else if (diamonds.size() >= 5) {
    		setFlushCards(diamonds);
    		determineStraight(diamonds, true);
    	} else {
    		// Check for a straight in the remaining cards
    		determineStraight(uniqueCardNumbers, false);
    	}
    	
    	scoreRemainingCards();
	}
	
	/**
	 * Counts the number of cards for each card face.
	 * 
	 * @param cardNumber a card number to be counted.
	 */
	private void countCards(Integer cardNumber) {
		Integer count = cardCount.get(cardNumber);
		
		if (count == null) {
			cardCount.put(cardNumber, 1);
		} else {
			cardCount.put(cardNumber, cardCount.get(cardNumber)+1);
		}
	}
	
	/**
	 * Iterate through the card count and score the user depending on
	 * their best 5 cards.
	 */
	private void scoreRemainingCards() {
		// If it has already be determined that the player
		// has a royal (10) or straight flush (9), the score is set.
		if (bestFiveCardRank >= 9) return;
		
		// Check for 4 of a kind
		TreeSet<Integer> quadruple = new TreeSet<Integer>(Collections.reverseOrder());
		TreeSet<Integer> triple = new TreeSet<Integer>(Collections.reverseOrder());
		TreeSet<Integer> pairs = new TreeSet<Integer>(Collections.reverseOrder());
		
		ArrayList<Integer> allNumbers = new ArrayList<Integer>();
		
		for (Map.Entry<Integer, Integer> count : cardCount.entrySet()) {
			allNumbers.add(count.getKey());
			
			if (count.getValue() == 2) {
				pairs.add(count.getKey());
				
				// This number should be added to the number list 1 more time.
				// Total = 2 times
				allNumbers.add(count.getKey());
			} else if (count.getValue() == 3) {
				triple.add(count.getKey());
				
				// This number should be added to the number list 2 more time.
				// Total = 3 times
				allNumbers.add(count.getKey());
				allNumbers.add(count.getKey());
			} else if (count.getValue() == 4) {
				quadruple.add(count.getKey());
				
				// This number should be added to the number list 3 more time.
				// Total = 4 times
				allNumbers.add(count.getKey());
				allNumbers.add(count.getKey());
				allNumbers.add(count.getKey());
			}
		}
		
		Collections.sort(allNumbers, Collections.reverseOrder());
		
		// If there is a Four of a Kind (8), set the score, highest card, and
		// possible kickers for future ties.
		if (quadruple.size() > 0) {
			bestFiveCardRank = 8;
			highestCard = quadruple.first();
			
			//Find the possible kickers, there can only be 1.
			for (Integer i : allNumbers) {
				if (i != highestCard) {
					kickers.add(i);
					break;
				}
			}
			return;
		}
		
		// If there are triples and pairs, then there is a full house (7)
		// Either 2 triples or 1 triple and multiple pairs.
		if ((triple.size() == 2)||((triple.size() > 0)&&(pairs.size() > 0))) {
			bestFiveCardRank = 7;
			Iterator<Integer> iterate = triple.iterator();
			highestCard = iterate.next();
			
			if (triple.size() == 2) {
				// If there was a second triple number, that is the pair
				// to complete the full house.
				secondPairCard = iterate.next();
			} else {
				// The highest pair is used to complete the full house
				secondPairCard = pairs.first();
			}
			return;
		}
		
		// If a Flush (6) or Straight (5) is found, all the proper settings
		// are noted for tie breakers.
		if ((bestFiveCardRank == 6)||(bestFiveCardRank == 5)) {
			return;
		}
		
		// If a triple is found with no double, there is a Three of a Kind (4)
		if (triple.size() > 0) {
			bestFiveCardRank = 4;
			highestCard = triple.first();
			
			// Find possible kickers, there can be up to 3.
			for (Integer i : allNumbers) {
				if (i != highestCard) {
					kickers.add(i);
					if (kickers.size() == 3) break;
				}
			}			
			return;
		}
		
		// If at least two pairs are found, it scores as a Two Pair (3)
		if (pairs.size() >= 2) {
			bestFiveCardRank = 3;
			
			Iterator<Integer> iterator = pairs.iterator();
			highestCard = iterator.next();
			secondPairCard = iterator.next();
			
			//Find the possible kickers, there can only be 1.
			for (Integer i : allNumbers) {
				if ((i != highestCard)&&(i != secondPairCard)) {
					kickers.add(i);
					break;
				}
			}
			return;
		}
		
		// If at least 1 pair is found, it scores as Pair (2)
		if (pairs.size() == 1) {
			bestFiveCardRank = 2;
			highestCard = pairs.first();

			// Find possible kickers, there can be up to 3.
			for (Integer i : allNumbers) {
				if (i != highestCard) {
					kickers.add(i);
					if (kickers.size() == 3) break;
				}
			}			
			return;			
		}
		
		// Lastly, just compare the first 5 highest cards.
		bestFiveCardRank = 1;
		highestCard = allNumbers.get(0);
		
		for (int i=1; i<5; i++) {
			kickers.add(allNumbers.get(i));
		}
	}
	
	/**
	 * Gets the player's name.
	 * 
	 * @return player's name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the highest card in a specialty hand.
	 * 
	 * @return highest card in a specialty hand.
	 */
	public int getHighestCard() {
		return highestCard;
	}
	
	/**
	 * Gets the highest specialty hand this player can achieve.
	 * 
	 * Specialties are ranked as follows:
	 * 1 - High Card
	 * 2 - Pair
	 * 3 - Two Pair
	 * 4 - Three of a Kind
	 * 5 - Straight
	 * 6 - Flush
	 * 7 - Full House
	 * 8 - Four of a Kind
	 * 9 - Straight Flush
	 * 10 - Royal Flush
	 * 
	 * @return the highest specialty hand this player can have.
	 */
	public int getHighestSpecialty() {
		return bestFiveCardRank;
	}
	
	/**
	 * Translates the player's best hand into text.
	 * 
	 * @param rank the player's current rank out of all players
	 * @param kicker the kicker card used to break a tie.
	 */
	public void translateScore(int rank, Integer highestKicker, boolean isTie) {
		String specialty = String.valueOf(rank) + ": " + name + " - ";
		switch (bestFiveCardRank) {
			case 1: specialty += "High Card, " + CardUtils.translateCardNumber(highestCard);
					break;
			case 2: specialty += "Pair, " + CardUtils.translateCardNumber(highestCard);
					break;
			case 3: specialty += "Two Pair, " + CardUtils.translateCardNumber(highestCard)
									+ " " + CardUtils.translateCardNumber(secondPairCard);
					break;
			case 4: specialty += "Three of a Kind, " + CardUtils.translateCardNumber(highestCard);
					break;
			case 5: specialty += "Straight, " + CardUtils.translateCardNumber(highestCard);
					break;
			case 6: specialty += "Flush,";
					for (int i=0; i<5; i++) {
						specialty += " " + CardUtils.translateCardNumber(flush[i]);
					}
					break;
			case 7: specialty += "Full House, " + CardUtils.translateCardNumber(highestCard)
								+ " over " + CardUtils.translateCardNumber(secondPairCard);
					break;
			case 8: specialty += "Four of a Kind, " + CardUtils.translateCardNumber(highestCard);
					break;
			case 9: specialty += "Straight Flush, " + CardUtils.translateCardNumber(highestCard);
					break;
			case 10: specialty += "Royal Flush";
					 break;
		}
		
		if (highestKicker > 0) specialty += ", with a kicker of " + CardUtils.translateCardNumber(highestKicker);
			
		if ((rank == 1)&&(isTie)) {
			specialty += ", tie so pot is split";
		}
		
		System.out.println(specialty);
	}
	
	/**
	 * Gets the second pair card value, if the highest specialty hand is a two pair.
	 * Indicates the highest pair value, if the specialty hand is a full house.
	 * 
	 * @return the second pair card value.
	 */
	public int getSecondPairCard() {
		return secondPairCard;
	}
	
	public ArrayList<Integer> getKickers()	{
		return kickers;
	}
	
	/**
	 * Saves the flush cards to be used for compare in case of tie.
	 * If a flush is set, highestSpecialty = 6 (Flush)
	 * Import the SortedSet in reverse
	 * 
	 * @param cards sets the cards that are in the flush.
	 */
	public void setFlushCards(TreeSet<Integer> cards) {
		flush = new Integer[cards.size()];
		flush = ((TreeSet<Integer>) cards.descendingSet()).toArray(flush);
		bestFiveCardRank = 6;
	}
	
	/**
	 * Gets all the cards in a flush in descending order.
	 * 
	 * @return all cards in the flush in descending order.
	 */
	public Integer[] getFlushCards() {
		return flush;
	}
    
    /**
     * Iterate through the list of sorted card numbers and determine if there are
     * at least 5 consecutive cards. If so, note that as the player's specialty hand.
     * 
     * This will determine if the player has a royal flush, straight flush, or a straight.
     * 
     * @param cards an ordered list representation of the cards
     * @param player the player whose score is calculated
     * @return an updated player profile if a straight is found
     */
    private void determineStraight(TreeSet<Integer> cards, boolean isFlush) {
    	// If the cards contain an A (indicated by 14) it is also counted as 1 in a baby straight.
    	if (cards.contains(14)) cards.add(1);
    	
    	// Iterate through the sorted list to find a sequence of at least 5 cards.
    	int sequenceCount = 0;
    	Iterator<Integer> value = cards.iterator();
    	Integer lastNumber = value.next();
    	while (value.hasNext()) {
    		Integer nextNumber = value.next();
    		if ((nextNumber - lastNumber) == 1) {
    			// If the next number is a consecutive number,
    			// increase the sequence count.
    			sequenceCount++;
    		} else {
    			// If the next number is not a consecutive number,
    			// restart the sequence count.
    			sequenceCount = 0;
    		}
    		
    		// Once the sequence is at least 5 digits long, record the highest
    		// value in the sequence.
    		if (sequenceCount >= 4) {
    			highestCard = nextNumber;
    		}
    		
    		// Set the current number as the last number and continue iterating 
    		// through the list.
			lastNumber = nextNumber;
    	}
    	
    	// If the highest number is 14 and is a flush, it is a royal flush (Specialty #10).
    	// If the highest number is larger than 0...
    	// 		- and is a flush, it is a straight flush (Specialty #9).
    	//		- otherwise, it is just a straight (Specialty #5)
    	if ((highestCard == 14)&&isFlush) bestFiveCardRank = 10;
    	else if (highestCard >= 0) {
    		if (isFlush) bestFiveCardRank = 9;
    		else bestFiveCardRank = 5;
    	}
    }	
}
