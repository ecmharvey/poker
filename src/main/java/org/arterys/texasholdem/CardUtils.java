package org.arterys.texasholdem;

import java.util.regex.Pattern;

public class CardUtils {
	static boolean isValidCard(String card) {
		return Pattern.matches("^[2-9tjqka][hsdc]$", card.toLowerCase());
	}

    /**
     * The following is a help document noting how cards should be notated.
     */
    static void printHelp() {
        System.out.println("**************************************************");
        System.out.println("Each card should be 2 characters, first being a card number followed by the cardface.");
        System.out.println("\nCard Numbers:");
        System.out.println("- 1 through 9: representing their respective values.");
        System.out.println("- 10: T");
        System.out.println("- Jack: J");
        System.out.println("- Queen: Q");
        System.out.println("- King: K");
        System.out.println("- Ace: A");
        System.out.println("\nCard Faces:");
        System.out.println("- Hearts: H");
        System.out.println("- Spades: S");
        System.out.println("- Diamonds: D");
        System.out.println("- Clubs: C");
        System.out.println("**************************************************");
    }
	
	/**
	 * Returns the string version of the given number.
	 * 
	 * Card Numbers Range:
     *  - 2 through 9: representing their respective values.
     *  - 10: ten
     *  - 11: jack
     *  - 12: queen
     *  - 13: king
     *  - 14: ace
	 * 
	 * @param cardNumber the card number to convert to a string
	 * @return string value of the given card number
	 */
	static String translateCardNumber(Integer cardNumber) {
		String cardFace = new String();
		
		switch (cardNumber) {
			case 2: return "2";
			case 3: return "3";
			case 4: return "4";
			case 5: return "5";
			case 6: return "6";
			case 7: return "7";
			case 8: return "8";
			case 9: return "9";
			case 10: return "10";
			case 11: return "Jack";
			case 12: return "Queen";
			case 13: return "King";
			case 14: return "Ace";
		}
		
		return cardFace;
	}
	

    /**
     * Converts a given string representation of a number to a numerical value.
     * 
     *  - 2 through 9: representing their respective values.
     *  - t: 10
     *  - j: 11
     *  - q: 12
     *  - k: 13
     *  - a: 14
     * 
     * @param number
     * @return
     */
    static Integer convertCardNumberToInteger(String number) {    	
    	if (number.equalsIgnoreCase("t")) return 10;
    	else if (number.equalsIgnoreCase("j")) return 11;
    	else if (number.equalsIgnoreCase("q")) return 12;
    	else if (number.equalsIgnoreCase("k")) return 13;
    	else if (number.equalsIgnoreCase("a")) return 14;
    	else return Integer.valueOf(number);
    }
}
