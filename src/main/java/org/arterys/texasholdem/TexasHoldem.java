package org.arterys.texasholdem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public final class TexasHoldem {
	private static String[] communityCards;
	private static ArrayList<String[]> players;
	private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	public TexasHoldem() {
	}

	/**
	 * Runs the main features of the Texas Hold'em game. Will continue until a user
	 * types 'exit'.
	 * 
	 * @param args
	 *            The arguments of the program.
	 */
	public static void main(String[] args) {
		while (true) {
			runGame();
		}
	}
	
	/**
	 * Starts another round of gameplay.
	 */
	private static void runGame() {
		communityCards = null;
		players = new ArrayList<String[]>();

		System.out.println("-------------------------------------------");
		System.out.println("Let's Play Texas Hold'em!");
		System.out.println("Please enter the community cards followed by each player's hand.");
		System.out.println("Enter \'help\' for a reference guide.");

		// Read input from the user.
		readConsole();
		
		if (communityCards != null) {
			System.out.println("... validating entries...");
	
			// Given the user input, validate all the cards entered.
			// Also, gives the user a chance to fix invalid input.
			validateEntries();
	
			System.out.println("\nAnd the results are...");
	
			ArrayList<Player> scoredPlayers = new ArrayList<Player>();
			// Look at each player's card with the community cards
			// and score each player.
			for (String[] player : players) {
				Player newPlayer = new Player(player, communityCards);
				scoredPlayers.add(newPlayer);
			}
	
			// Sort the players in descending order by score.
			Collections.sort(scoredPlayers, new CompareFunctions.SortPlayersByScore());
			
			printRanks(scoredPlayers);
		
			System.out.println("-------------------------------------------");
			System.out.println("Would you like to play again? (y/n)");
			if (!readYesNo()) {
				System.out.println("Goodbye!");
				System.exit(1);
			}
		} else {
			System.out.println("-------------------------------------------");
			System.out.println("Are you sure you want to quit? (y/n)");
			if (readYesNo()) {
				System.out.println("Goodbye!");
				System.exit(1);
			}
		}
	}

	/**
	 * As the user enters the cards in play, read the information coming from the
	 * console. The first line will be the community cards and subsequent lines
	 * should be player information.
	 */
	private static void readConsole() {
		String line;
		boolean firstLine = true;
		try {
			while (((line = reader.readLine()) != null) && (line.length() > 0)) {
				if (line.equalsIgnoreCase("help")) {
					CardUtils.printHelp();
				} else {
					String[] lines = line.split("\\s");

					if (firstLine) {
						if (lines.length != 5) {
							System.out
									.println("There should be 5 community cards. Please re-enter the community cards.");
						} else {
							communityCards = lines;
							firstLine = false;
						}
					} else {
						if (lines.length != 3) {
							System.out.println(
									"Each player entry should consist of the player's first name plus 2 cards. Please re-enter the player's information.");
						} else {
							players.add(lines);
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Notifies the program when the user submitted a yes or a no or defaults
	 * invalid answers to yes.
	 * 
	 * @return user response, either yes or no. Default is yes.
	 */
	private static boolean readYesNo() {
		String line;
		try {
			while (((line = reader.readLine()) != null) && (line.length() > 0)) {
				if (line.equalsIgnoreCase("n")) {
					return false;
				} else if (line.equalsIgnoreCase("y")) {
					return true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * Iterate through the cards to ensure they are properly formatted.
	 */
	private static void validateEntries() {
		// Community Cards
		for (int i = 0; i < communityCards.length; i++) {
			String card = validateCard(communityCards[i], "community card");
			communityCards[i] = card;
		}

		// Player Cards
		for (String[] playerCard : players) {
			// Validate the player's first card
			playerCard[1] = validateCard(playerCard[1], "card for " + playerCard[0]);
			// Validate the player's second card
			playerCard[2] = validateCard(playerCard[2], "card for " + playerCard[0]);
		}
	}

	/**
	 * Ensure that the given card is valid. Prompt the user if it is not.
	 * 
	 * Each card should only be 2 values.
	 * 
	 * Card Numbers Range: - 2 through 9: representing their respective values. -
	 * 10: T - Jack: J - Queen: Q - King: K - Ace: A
	 * 
	 * Card Faces: - Hearts: H - Spades: S - Diamonds: D - Clubs: C
	 * 
	 * @param card
	 *            the current card to validate.
	 * @param invalidType
	 *            indicates whether a card is a community card of player card.
	 * @return a valid card entry.
	 */
	private static String validateCard(String card, String invalidType) {
		while (!CardUtils.isValidCard(card)) {
			System.out.println(card + " is an invalid " + invalidType
					+ ". Please enter a valid card number (Type \'help\' for a reference guide): ");

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			try {
				card = reader.readLine();

				if (card.equalsIgnoreCase("help")) {
					CardUtils.printHelp();
					System.out.println("Please enter a valid card number: ");
					card = reader.readLine();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return card;
	}

	/**
	 * Given a list of sorted players, sorted in descending score order, print out
	 * the player's highest rank along with any kicker cards.
	 * 
	 * @param scoredPlayers list of sorted players in descending order by rank.
	 */
	private static void printRanks(ArrayList<Player> scoredPlayers) {
		int rank = 1;
		boolean isTie = false;
		boolean previousWasTie = false;
		Integer highestKicker = 0;
		
		while (scoredPlayers.size() > 1) {
			Player firstPlayer = scoredPlayers.get(0);
			Player secondPlayer = scoredPlayers.get(1);
			Integer firstHandType = firstPlayer.getHighestSpecialty();
			isTie = false;
			highestKicker = 0;
			int cmp = 0;
			
			// Compares to determine whether the players are tied or not
			// Used to determine rank.
			// Case 10: Royal Flush - all users tie with this score.
			if (firstHandType == secondPlayer.getHighestSpecialty()) {
				switch (firstHandType) {
				case 9: // Straight Flush
					// - Player with the highest card wins a tie.
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();
					break;
				case 8: // Four of a Kind
					// - Player with highest 4 of a kind wins.
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();

					// - Check 1 kicker in case of tie.
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(0) - secondPlayer.getKickers().get(0);
						
						highestKicker = firstPlayer.getKickers().get(0);
					}
					break;
				case 7: // Full House
					// - Player with the highest triple wins.
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();

					// - If there is a tie, compare the pair card.
					if (cmp == 0) {
						cmp = firstPlayer.getSecondPairCard() - secondPlayer.getSecondPairCard();
					}
					break;
				case 6: // Flush
					// - Player with the highest flush cards win.
					// - If all the cards match, there is a tie.
					Integer[] firstFlush = firstPlayer.getFlushCards();
					Integer[] secondFlush = secondPlayer.getFlushCards();
					
					for (int i=0; i<5; i++) {
						if (cmp == 0) {
							cmp = firstFlush[i] - secondFlush[i];
						}
					}
					break;
				case 5: // Straight
					// - Player with the highest card wins a tie
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();
					break;
				case 4: // Three of a Kind
					// - Player with the highest triple card wins.
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();
					
					// - Check 2 kickers in case of tie.
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(0) - secondPlayer.getKickers().get(0);
						
						highestKicker = firstPlayer.getKickers().get(0);
						if (cmp != 0) {
							break;
						}
					}
					
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(1) - secondPlayer.getKickers().get(1);
						
						if (cmp != 0) {
							highestKicker = firstPlayer.getKickers().get(1);
						}
					}
					break;
				case 3: // Two Pairs
					// The highest pair cards win.
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();

					// - If there is a tie, check the second pair.
					if (cmp == 0) {
						cmp = firstPlayer.getSecondPairCard() - secondPlayer.getSecondPairCard();
					}
					
					// - Check 1 kicker in case of tie.
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(0) - secondPlayer.getKickers().get(0);
						
						highestKicker = firstPlayer.getKickers().get(0);
					}
					break;
				case 2: // Single Pair
					// The highest pair card wins.
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();
					
					// - Check 3 kickers in case of tie.
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(0) - secondPlayer.getKickers().get(0);

						highestKicker = firstPlayer.getKickers().get(0);
						if (cmp != 0) {
							break;
						}
					}
					
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(1) - secondPlayer.getKickers().get(1);
						
						if (cmp != 0) {
							highestKicker = firstPlayer.getKickers().get(1);
							break;
						}
					}
					
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(2) - secondPlayer.getKickers().get(2);
						
						if (cmp != 0) {
							highestKicker = firstPlayer.getKickers().get(2);
						}
					}
					break;
				case 1: // High Card
					// - Player with the highest card wins.
					cmp = firstPlayer.getHighestCard() - secondPlayer.getHighestCard();
					
					// - Check 4 kickers in case of tie.
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(0) - secondPlayer.getKickers().get(0);

						highestKicker = firstPlayer.getKickers().get(0);
						if (cmp != 0) {
							break;
						}
					}
					
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(1) - secondPlayer.getKickers().get(1);
						
						if (cmp != 0) {
							highestKicker = firstPlayer.getKickers().get(1);
							break;
						}
					}
					
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(2) - secondPlayer.getKickers().get(2);
						
						if (cmp != 0) {
							highestKicker = firstPlayer.getKickers().get(2);
							break;
						}
					}
					
					if (cmp == 0) {
						cmp = firstPlayer.getKickers().get(3) - secondPlayer.getKickers().get(3);
						
						if (cmp != 0) {
							highestKicker = firstPlayer.getKickers().get(3);
						}
					}
					break;					
				}
				
				if (cmp == 0) isTie = true;
			}
			
			// Note whether the current first player was tied with its previous
			// player comparison. This is most important when noting when the pot is tied.
			firstPlayer.translateScore(rank, highestKicker, isTie||previousWasTie);
			scoredPlayers.remove(firstPlayer);
			
			// Increase the rank if there was no tie between the two compared players.
			// Take note on whether this iteration resulted in a tie or not.
			if (!isTie) {
				rank++;
				previousWasTie = false;
				highestKicker = 0;
			} else {
				previousWasTie = true;
			}
		}
		
		scoredPlayers.get(0).translateScore(rank, highestKicker, isTie);
	}
}
