package org.arterys.texasholdem;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

/**
 * Tests each time of specialty hand a user can have.
 */
public class AppTest {
    
    @Test
    public void testPlayerScoring() {
    	assertRoyalFlush("ah", "kh", "qh", "jh", "th", "5h", "2h");
    
    	assertStraightFlush(13, "9s", "kh", "qh", "jh", "th", "9h", "9c");
        
    	assertFourOfAKind(9, Arrays.asList(13), "9s", "9h", "9c", "9d", "kh", "2h", "5h");
    	
    	assertFourOfAKind(9, Arrays.asList(7), "9s", "9h", "9c", "9d", "7h", "2h", "2c");
    	
    	assertFullHouse(9, 13, "9s", "9h", "9c", "7d", "kh", "7h", "kc");
    	
    	assertFullHouse(9, 7, "9s", "9h", "9c", "7d", "7c", "7h", "kc");

    	Integer[] flush = null;
    	flush = new Integer[] {13, 10, 8, 7, 6, 3, 2};
    	assertFlush(flush, "2h", "kh", "7h", "3h", "th", "8h", "6h");
    	
    	flush = new Integer[] {13, 10, 8, 7, 6};
    	assertFlush(flush, "2d", "kh", "7h", "2c", "th", "8h", "6h");
    	
    	assertStraight(9, "9h", "8d", "7c", "6h", "5s", "3c", "js");
    	
    	assertStraight(11, "9h", "8d", "7c", "6h", "5s", "tc", "js");

    	assertThreeOfAKind(4, Arrays.asList(9,8,6), "4d","4s","4c","9d","6h","8s","2c");

    	assertThreeOfAKind(9, Arrays.asList(12,10,6), "9d","9s","9c","qd","6h","ts","2c");

    	assertTwoPair(9, 5, Arrays.asList(14), "9d","5c","4c","9s","jd","ad","5s");
    	
    	assertPair(13, Arrays.asList(12,9,6), "kd","2c","6h","4s","kh","qc","9d");

    	assertHighestCard(14, Arrays.asList(12, 9, 8, 6), "9c","6d","ad","8c","4h","2s","qh");
    }
    
    /**
     * Given the cards passed in, a royal flush should be found
     * as the highest five card hand.
     * 
     * @param cards list of cards that should be evaluated.
     */
    private static void assertRoyalFlush(String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(10, player.getHighestSpecialty());
    }
    
    /**
     * Given the cards passed in, a straight flush should be found
     * - the highest card should be equal to the highest card in the straight
     * 
     * @param expectedHighestCard highest card that was found in the straight
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertStraightFlush(Integer expectedHighestCard, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(9, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    }
    
    /**
     * Given the cards passed in, a four of a kind should be found
     * - the highest card should be equal to the highest card
     * - The kicker list should be only what is relevant
     * 
     * @param expectedHighestCard highest card in the hand
     * @param expectedKickers kickers to compare in case of tie with another player
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertFourOfAKind (Integer expectedHighestCard, List<Integer> expectedKickers, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(8, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    	assertArrayEquals(expectedKickers.toArray(), player.getKickers().toArray());
    }
    
    /**
     * Given the cards passed in, a full house should be found
     * - the highest card should be equal to the triple number
     * - the second highest number should be the second highest pair number
     * 
     * @param expectedHighestCard value of the triple
     * @param expectedSecondHighest highest pair value
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertFullHouse (Integer expectedHighestCard, Integer expectedSecondHighest, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(7, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    	assertEquals((Integer) expectedSecondHighest, (Integer) player.getSecondPairCard());
    }
    
    /**
     * Given the cards passed in, a flush should be found
     * 
     * @param expectedFlush expected list of cards that create a flush
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertFlush (Integer[] expectedFlush, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(6, player.getHighestSpecialty());
    	assertArrayEquals(expectedFlush, player.getFlushCards());
    }
    

    
    /**
     * Given the cards passed in, a straight should be found
     * - the highest card should be equal to the highest card in the straight
     * 
     * @param expectedHighestCard highest card that was found in the straight
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertStraight(Integer expectedHighestCard, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(5, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    }
    
    /**
     * Given the cards passed in, a three of a kind should be found
     * - the highest card should be equal to the highest card
     * - The kicker list should be only what is relevant
     * 
     * @param expectedHighestCard highest card in the hand
     * @param expectedKickers kickers to compare in case of tie with another player
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertThreeOfAKind (Integer expectedHighestCard, List<Integer> expectedKickers, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(4, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    	assertArrayEquals(expectedKickers.toArray(), player.getKickers().toArray());
    }
    
    /**
     * Given the cards passed in, two pairs should be found
     * - the highest card should be equal to the largest pair number
     * - the second highest number should be the second highest pair number
     * - The kicker list should be only what is relevant
     * 
     * @param expectedHighestCard value of the triple
     * @param expectedSecondHighest highest pair value
     * @param expectedKickers kickers to compare in case of tie with another player
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertTwoPair (Integer expectedHighestCard, Integer expectedSecondHighest, List<Integer> expectedKickers, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(3, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    	assertEquals((Integer) expectedSecondHighest, (Integer) player.getSecondPairCard());
    	assertArrayEquals(expectedKickers.toArray(), player.getKickers().toArray());
    }
    
    /**
     * Given the cards passed in, one pair should be found
     * - the highest card should be equal to the highest card
     * - The kicker list should be only what is relevant
     * 
     * @param expectedHighestCard highest card in the hand
     * @param expectedKickers kickers to compare in case of tie with another player
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertPair (Integer expectedHighestCard, List<Integer> expectedKickers, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(2, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    	assertArrayEquals(expectedKickers.toArray(), player.getKickers().toArray());
    }
    
    /**
     * Given the cards passed in, one pair should be found
     * - the highest card should be equal to the highest card
     * - The kicker list should be only what is relevant
     * 
     * @param expectedHighestCard highest card in the hand
     * @param expectedKickers kickers to compare in case of tie with another player
     * @param cards all the cards passed in to be evaluated
     */
    private static void assertHighestCard (Integer expectedHighestCard, List<Integer> expectedKickers, String... cards) {
    	String[] playerCards = new String[] {"newPlayer", cards[0], cards[1]};
    	String[] communityCards = new String[] {cards[2], cards[3], cards[4], cards[5], cards[6]};
    	
    	Player player = new Player(playerCards, communityCards);
    	assertEquals(1, player.getHighestSpecialty());
    	assertEquals((Integer) expectedHighestCard, (Integer) player.getHighestCard());
    	assertArrayEquals(expectedKickers.toArray(), player.getKickers().toArray());
    }
}
