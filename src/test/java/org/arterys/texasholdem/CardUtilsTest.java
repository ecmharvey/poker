package org.arterys.texasholdem;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests all the utility classes that perform changes to cards.
 */
public class CardUtilsTest {

	@Test
	public void testIsCardValid() {
		// card entry is too many characters, with valid number and suit
		assertFalse(CardUtils.isValidCard("3hc"));
		
		// card entry is too few characters, with valid number
		assertFalse(CardUtils.isValidCard("3"));
		
		// card face is too low, with valid suit
		assertFalse(CardUtils.isValidCard("1h"));
		
		// card face is within 2-9, with valid suit
		assertTrue(CardUtils.isValidCard("2h"));
		
		// card face is a face card, with valid suit
		assertTrue(CardUtils.isValidCard("jh"));
		
		// card face is an invalid face card, with valid suit
		assertFalse(CardUtils.isValidCard("wh"));
		
		// card suit is a valid entry, with valid number
		assertTrue(CardUtils.isValidCard("2h"));
		
		// card suit is an invalid entry, with valid number
		assertFalse(CardUtils.isValidCard("2r"));
	}

	@Test
	public void testTranslateCard() {
		// Valid card number value
		assertEquals("2", CardUtils.translateCardNumber(2));
		
		// Valid card face value
		assertEquals("Jack", CardUtils.translateCardNumber(11));
		
		// Invalid number value
		assertEquals("", CardUtils.translateCardNumber(1));
	}
	
	@Test
	public void testConvertCardNumber() {
		// Convert ten to an integer
		assertEquals((Integer) 10, CardUtils.convertCardNumberToInteger("t"));
		
		// Convert string "5" to an integer
		assertEquals((Integer) 5, CardUtils.convertCardNumberToInteger("5"));
		
		// Convert string "j" to an integer
		assertEquals((Integer) 11, CardUtils.convertCardNumberToInteger("j"));
	}
}
